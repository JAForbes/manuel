/* eslint-disable fp/no-mutating-methods, fp/no-mutation */
const test = require('tape')
const manuel = require('../index.js')
const m = require('mithril')

const render =
	require('mithril-node-render')

const R = require('ramda')

function Model(){

	return {
		model: {
			list: [[]]
			,input: ['']
			,chosen: [null]
			,open: [false]
			,highlighted: [null]
		}
		,modelKeys: {
			list: 'list'
			,input: 'input'
			,chosen: 'chosen'
			,open: 'open'
			,highlighted: 'highlighted'
		}
	}

}
function mithrilAutocomplete({model, modelKeys}){
	return {
		autocomplete:
			manuel({
				hyperscript: m
				,get: k => model[k][0]

				,set:
					/* istanbul ignore next */
					(k,v) => model[k].unshift(v)
			})
		,model
		,modelKeys
	}
}

test('Queries Mutable', function(t){

	const { model, modelKeys, autocomplete } =
		mithrilAutocomplete( Model() )


	var overrides = {
		minChars: 0
	}

	model.list.unshift([
		'Soma'
		,'Soy Beans'
		,'Soda'
	])

	const $input = manuel.queries.input(function(input){
		input.attrs.style = { backgroundColor: 'red' }
	})

	const $div = manuel.queries.root(function(root){
		root.attrs.className = 'blue'
	})

	const $li = manuel.queries.listItems(function(xs){
		xs.forEach(function(x){
			x.children[0] = x.children[0].toUpperCase()
		})
	})

	const $ul = manuel.queries.list(function(ul){
		ul.children = ul.children.concat(ul.children)
	})

	const out =

		$div(
			$input(
				$ul(
					$li(
						autocomplete(modelKeys, overrides)
					)
				)
			)
		)

	const expected =
		`<div class="blue">
			<input value="" style="background-color:red">
			<ul>
				<li class="">SODA</li>
				<li class="">SOMA</li>
				<li class="">SOY BEANS</li>
				<li class="">SODA</li>
				<li class="">SOMA</li>
				<li class="">SOY BEANS</li>
			</ul>
		</div>
		`.replace(/\t|\n/g, '')

	t.equals(
		expected, render(out), 'All the queries work'
	)

	t.end()
})

test('Queries Immutable', function(t){

	const { model, modelKeys, autocomplete } =
		mithrilAutocomplete( Model() )


	var overrides = {
		minChars: 0
	}

	model.list.unshift([
		'Soma'
		,'Soy Beans'
		,'Soda'
	])

	const $input = manuel.queries.input(
		R.assocPath(['attrs','style', 'backgroundColor'], 'red')
	)

	const $div = manuel.queries.root(
		R.assocPath(['attrs', 'className'], 'blue')
	)

	const $li = manuel.queries.listItems(
		R.map(
			R.over( R.lensProp('children'), R.map(R.toUpper) )
		)
	)

	const $ul = manuel.queries.list(
		R.over( R.lensProp('children'), xs => xs.concat(xs) )
	)

	const out =

		$div(
			$input(
				$ul(
					$li(
						autocomplete(modelKeys, overrides)
					)
				)
			)
		)

	const expected =
		`<div class="blue">
			<input value="" style="background-color:red">
			<ul>
				<li class="">SODA</li>
				<li class="">SOMA</li>
				<li class="">SOY BEANS</li>
				<li class="">SODA</li>
				<li class="">SOMA</li>
				<li class="">SOY BEANS</li>
			</ul>
		</div>
		`.replace(/\t|\n/g, '')

	t.equals(
		expected, render(out), 'All the queries work'
	)

	t.end()
})

// istanbul ignore next
// eslint-disable-next-line
process.on('unhandledRejection', r => console.error(r));